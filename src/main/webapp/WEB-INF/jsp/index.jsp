<!DOCTYPE HTML>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Pragma" content="no-cache"> 
    <meta http-equiv="Cache-Control" content="no-cache">

    
    <title>Hospital | Home</title>
    
    <link href="static/css/bootstrap.min.css" rel="stylesheet">
     <link href="static/css/style.css" rel="stylesheet">
    
    <!--[if lt IE 9]>
		<script src="static/js/html5shiv.min.js"></script>
		<script src="static/js/respond.min.js"></script>
	<![endif]-->
</head>
<body>

	<div role="navigation">
		<div class="navbar navbar-inverse">
			<a href="/" class="navbar-brand">Home</a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="new-task">Daftar Pasien</a></li>
					<li><a href="all-tasks">Data Pasien</a></li>
					<li><a href="new-task">Input Doctor</a></li>
					<li><a href="all-tasks">Data Doctor</a></li>
					<li><a href="all-tasks">Report</a></li>
				</ul>
			</div>
		</div>
	</div>
	
	<c:choose>
		<c:when test="${mode == 'MODE_HOME'}">
			<div class="container" id="homeDiv">
				<div class="jumbotron text-center">
					<h1>Welcome to Task Manager</h1>
				</div>
			</div>

		</c:when>
		<c:when test="${mode == 'MODE_TASKS'}">
			<div class="container text-center" id="tasksDiv">
				<h3>My Tasks</h3>
				<hr>
				<div class="table-responsive">
					<table class="table table-striped table-bordered text-left">
						<thead>
							<tr>
								<th>Id</th>
								<th>Name</th>
								<th>Address</th>
								<th>date of birth</th>
								<th>Gender</th>
								<th>Status</th>
								<th>Keluhan</th>
								<th>date of entry</th>
								<th>Payment</th>
								<th>Phone</th>

								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="task" items="${tasks}">
								<tr>
									<td>${task.id}</td>
									<td>${task.name}</td>
									<td>${task.alamat}</td>
									<td>${task.tanggallahir}</td>
									<td>${task.gender}</td>
									<td>${task.status}</td>
									<td>${task.description}</td>
									<td><fmt:formatDate pattern="yyyy-MM-dd" value="${task.dateCreated}"/></td>
									<td>${task.pembayaran}</td>
									<td>${task.noTelfon}</td>
									<td><a href="update-task?id=${task.id}"><span class="glyphicon glyphicon-pencil"></span></a></td>
									<td><a href="delete-task?id=${task.id}"><span class="glyphicon glyphicon-trash"></span></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</c:when>
		<c:when test="${mode == 'MODE_NEW' || mode == 'MODE_UPDATE'}">
			<div class="container text-center">
				<h3>Manage Task</h3>
				<hr>
				<form class="form-horizontal" method="POST" action="save-task">
					<input type="hidden" name="id" value="${task.id}"/>

					<div class="form-group">
						<label class="control-label col-md-3">Name</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="name" value="${task.name}"/>
						</div>				
					</div>
		
					<div class="form-group">
						<label class="control-label col-md-3">Alamat</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="alamat" value="${task.alamat}"/>
						</div>				
					</div>

					<div class="form-group">
						<label class="control-label col-md-3" >Tanggal Lahir</label>
						<div class="col-md-7">
							<input type="date" class="form-control" name="tanggallahir" value="${task.tanggallahir}"/>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3"> Gender</label>
						<div class="col-md-7">
						<label><input type="radio" id="button" class="form-control" name="gender" value='M'"${task.gender}"/>F</label>

							<label><input type="radio" id="button1" class="form-control" name="gender" value='F'"${task.gender}"/>M</label>
						</div>

					</div>
					<div class="form-group">
						<label class="control-label col-md-3">Status</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="status" value="${task.status}"/>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3">No Telfon</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="noTelfon" value="${task.noTelfon}"/>
						</div>
					</div>
				

					<div class="form-group">
						<label class="control-label col-md-3">Keluhan</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="description" value="${task.description}"/>
						</div>				
					</div>	
			
					<div class="form-group">
						<label class="control-label col-md-3">Pembayaran</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="pembayaran" value="${task.pembayaran}"/>
						</div>				
					</div>
					<p><input type="checkbox"> I accept Terms &amp; Condition</p>
					<div class="form-group">
						<input type="submit" class="btn btn-primary" value="Save"/>
					</div>
				</form>
			</div>
		</c:when>		
	</c:choose>

	<script src="static/js/jquery-1.11.1.min.js"></script>
    <script src="static/js/bootstrap.min.js"></script>
</body>
</html>