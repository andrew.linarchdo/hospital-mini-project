package bootsample.model;

import java.io.Serializable;
import java.util.Date;


import javax.persistence.*;

@Entity(name="pasien")
public class Task implements Serializable {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	private String name;
	private String description;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateCreated;
	private String alamat;
	private String tanggallahir;
	private String gender;
	private String noTelfon;
	private String status;
	private String pembayaran;

	public Task(){}



	public Task(String name, String description, Date dateCreated, String alamat, String tanggallahir, String gender, String no_Telfon, String status, String pembayaran) {
		this.name = name;
		this.description = description;
		this.dateCreated = dateCreated;
		this.alamat = alamat;
		this.tanggallahir = tanggallahir;
		this.gender = gender;
		this.noTelfon = noTelfon;
		this.status = status;
		this.pembayaran = pembayaran;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}

	public String getTanggallahir() {
		return tanggallahir;
	}

	public void setTanggallahir(String tanggallahir) {
		this.tanggallahir = tanggallahir;
	}


	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getNoTelfon() {
		return noTelfon;
	}

	public void setNoTelfon(String noTelfon) {
		this.noTelfon = noTelfon;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPembayaran() {
		return pembayaran;
	}

	public void setPembayaran(String pembayaran) {
		this.pembayaran = pembayaran;
	}

	@Override
	public String toString() {
		return "Task{" +
				"id=" + id +
				", name='" + name + '\'' +
				", description='" + description + '\'' +
				", dateCreated=" + dateCreated +
				", alamat='" + alamat + '\'' +
				", tanggallahir=" + tanggallahir +
				", gender=" + gender +
				", noTelfon=" + noTelfon +
				", status='" + status + '\'' +
				", pembayaran='" + pembayaran + '\'' +
				'}';
	}
}
